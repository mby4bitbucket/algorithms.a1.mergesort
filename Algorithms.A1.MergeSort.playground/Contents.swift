//: Playground - noun: a place where people can play

// p, q, r - indexes;
// q - is the end index of first subsequesnce [10,6,7,3,4,5] p: 1, q: 2, r: 5
func merge(arr: [Int], p: Int, q: Int, r: Int) -> [Int] {
    var res = arr
    
    let l1 = q - p + 1
    let l2 = r - q
    
    var arr1 = [Int]()
    var arr2 = [Int]()
    
    for i in 0..<l1 {
        arr1.append(arr[p + i])
    }
    
    for i in 0..<l2 {
        arr2.append(arr[q + 1 + i])
    }

    arr1.append(Int.max)
    arr2.append(Int.max)

    var i = 0
    var j = 0
    
    for k in p...r {
        if arr1[i] < arr2[j] {
            res[k] = arr1[i]
            i = i + 1
        } else {
            res[k] = arr2[j]
            j = j + 1
        }
    }
    
    return res
}
// p, r - indexes
// mergeSort(arr: [5,2,4,7,1,3,2,6], p: 0, r: 7)
func mergeSort(arr: [Int], p: Int, r: Int) -> [Int] {
    var res = arr
    if p < r {
        let q = (p + r) / 2
        res = mergeSort(arr: res, p: p, r: q)
        res = mergeSort(arr: res, p: q + 1, r: r)
        res = merge(arr: res, p: p, q: q, r: r)
    }
    
    return res
}

//merge(arr: [10,6,7,3,4,5], p: 1, q: 2, r: 5)
mergeSort(arr: [5,2,4,7,1,3,2,6], p: 0, r: 7)
